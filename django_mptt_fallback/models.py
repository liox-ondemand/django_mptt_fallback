from django.db.models import Q

from dirtyfields import DirtyFieldsMixin
from mptt.models import MPTTModel
from mptt.querysets import TreeQuerySet

from operator import and_, or_


class MPTTFallbackQuerySet(TreeQuerySet):
    def filter_descendants(self, *args, **kwargs):
        """
        Queryset method to include descendants that would fallback to a parent.

        In order to accomodate multiple filter kwargs, we build a list of
        results from each separate kwarg. Within each list, we gather a list
        of descendants that have a value defined for the filter in question.
        If it does, that means it won't be a valid result so we keep track of
        its id and its descendants. Once we have that list, we query for all
        descendants of the original list that have an empty value, excluding
        the results we already determined are invalid.

        Example:
            great_grand_parent.name = 'foo'
            grand_parent.name = '' (fallback to great_grand_parent and return 'foo')
            parent.name = '' (fallback to grand_parent and return 'foo')
            child.name = 'test'
            grand_child.name = '' (fallback to child and return 'test')

            >> Model.objects.filter_descendants(name='foo')
            >> [great_grand_parent, grand_parent, parent]

            >> Model.objects.filter_descendants(name='test')
            >> [child, grand_child]
        """
        fallback_results = {}
        non_fallback_filters = {}
        # Filter results according to each argument passed. If the filter is not
        # part of MPTTFallbackModel.fallback_fields, set it aside until later.
        for key, value in kwargs.iteritems():
            field_name = key
            if '__' in field_name:
                field_name = key.split('__')[0]
            if field_name in self.model.fallback_fields:
                fallback_results[field_name] = self.filter(Q(**{key: value}))
            else:
                non_fallback_filters.update({key: value})

        # Keep a list of lists containing object ids that conform to each queryset
        # in fallback_results. An object conforms if its fallback value matches what
        # was passed in as a filter.
        valid_id_lists = []
        for key, results in fallback_results.iteritems():
            # Gather all of the children that have a value where we are looking.
            # (This kills the fallback for children beyond it)
            # This means that we want to exclude them and their children.

            # Create a variable to store all descendants of the results so we
            # can avoid a query later
            descendants = results.get_descendants(include_self=True)

            try:
                # Begin a list of filters that will check for 'empty' values
                # (values we'll use inheritance for)
                empty_filter_args = [Q(**{'{0}__isnull'.format(key): True})]

                # When filtering on boolean fields, django will accept "truthy"
                # or "falsey" values.
                #
                # False equivalents:
                #   Model.objects.filter(boolean_field=False)
                #   Model.objects.filter(boolean_field='')
                #
                # True equivalents:
                #   Model.objects.filter(boolean_field=True)
                #   Model.objects.filter(boolean_field='foo')
                #
                # Because we allow '' to be an indicator of whether or not a
                # field should inherit, we don't want to use it as a filter
                # for boolean fields
                if 'boolean' not in self.model._meta.get_field(key).get_internal_type().lower():
                    empty_filter_args.append(Q(**{key: ''}))

                # This will condense all filters into a single Q object
                # using OR conditions
                empty_filters = reduce(
                    or_,
                    empty_filter_args
                )

                invalid_descendant_roots = descendants.exclude(
                    Q(id__in=results)
                ).exclude(
                     empty_filters
                )
            except ValueError:
                invalid_descendant_roots = descendants.exclude(
                    Q(id__in=results)
                ).exclude(
                    Q(**{'{0}__isnull'.format(key): True})
                )

            # Take the invalid children and gather a list of ids including them
            # and their children.
            invalid_descendants = self.filter(
                id__in=invalid_descendant_roots
            ).get_descendants(
                include_self=True
            ).values_list(
                'id',
                flat=True
            )

            # Filter out all of the children that don't match our query.
            valid_descendants = descendants.exclude(
                id__in=invalid_descendants
            ).values_list(
                'id',
                flat=True
            )

            valid_id_lists.append(valid_descendants)

        # Generate Q objects to make sure results exist in the list of results
        # for all filters
        fallback_query = None
        if len(valid_id_lists):
            # This will condense all filters into a single Q object
            # using AND conditions
            fallback_query = reduce(
                and_,
                [Q(**{'id__in': results}) for results in valid_id_lists]
            )

        # If there are any filters on non-fallback fields, apply them
        if non_fallback_filters:
            if fallback_query is None:
                fallback_query = Q(**non_fallback_filters)
            else:
                fallback_query &= Q(**non_fallback_filters)

        return self.filter(fallback_query)


class MPTTFallbackModel(MPTTModel, DirtyFieldsMixin):
    """
    An abstract model that is used to allow an object to fallback to data on
    its ancestors.

    EMPTY_VALUES - values that should result in a fallback to the parent
    fallback_fields - iterable containing the fields that should fallback
                      to the parent if the value is empty
    _original_field_values - a dictionary containing which fields were inherited
    _many_to_many_field_names - a list of many_to_many fields so we don't try
                                to update them when we store fallback fields.
    _explicit_edits = A set() that stores fields that were explicitly set so we
                  force the data being saved.
    """
    EMPTY_VALUES = ['', None]
    fallback_fields = None
    _original_field_values = None
    _explicit_edits = None
    _many_to_many_field_names = []

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super(MPTTFallbackModel, self).__init__(*args, **kwargs)
        self._original_field_values = {}
        self._explicit_edits = set()
        self._many_to_many_field_names = [field.name for field in self._meta.many_to_many]
        if self.is_child_node():
            self.gather_fallback_data()

    def __setattr__(self, attr_name, value, explicit=True):
        """
        We override this method because we want to force an update if the field
        is explicitly set to the same value that is defined on the parent.
        """
        super(MPTTFallbackModel, self).__setattr__(attr_name, value)
        if explicit and attr_name in self.fallback_fields:
            if self._original_field_values and attr_name in self._original_field_values:
                self._explicit_edits.update([attr_name])

    @property
    def fields_to_reset(self):
        """
        A property that will return the fields that should be reset to
        their initial value
        """
        soft_set_fields = {
            item for item in self._original_field_values.keys() if item not in self._explicit_edits
        }
        fields_to_reset = soft_set_fields - \
            set(self.get_dirty_fields(check_relationship=True).keys())
        return fields_to_reset

    def empty_fallback_fields(self):
        """
        Method used to empty out fallback fields. This can be used if you don't
        want to use fallback data in certain situations.
        """
        for field in self.fields_to_reset:
            if field not in self._many_to_many_field_names:
                self.__setattr__(field, self._original_field_values[field], explicit=False)

    def gather_fallback_data(self):
        """
        Method to populate the model's internals with fallback data from
        the parent.
        """
        fields = getattr(self, 'fallback_fields', None)
        # Empty out current inheritance data
        self._explicit_edits = set()
        self._original_field_values = {}
        if fields and self.parent:
            for field in fields:
                if field not in self._many_to_many_field_names:
                    self._store_field(field)
            self._reset_state()

    def _store_field(self, field):
        """
        Stores empty fields in internal dictionary so we can reset
        the model's state before we save. We don't want to save fallback
        data.
        """
        value = getattr(self, field)
        if value in self.EMPTY_VALUES:
            self._original_field_values[field] = value
            if field not in self._many_to_many_field_names:
                self.__setattr__(field, getattr(self.parent, field), explicit=False)

    def save(self, *args, **kwargs):
        """
        We only want to save objects that have actually been edited. We don't
        want to save data that is gathered from a hierarchy.
        """
        if self._original_field_values:
            self.empty_fallback_fields()
            super(MPTTFallbackModel, self).save(*args, **kwargs)
            self.gather_fallback_data()
        else:
            return super(MPTTFallbackModel, self).save(*args, **kwargs)

    def _reset_state(self):
        """
        Reset the object to only contain data that is saved in the database.
        """
        self._original_state = self._as_dict(check_relationship=True)

    def get_many_to_many_fallback(self, relationship):
        """
        Method to access many_to_many fields on an MPTTFallbackModel. If
        the many to many field is empty on the child, it will access the
        parent's.
        """
        if getattr(self, relationship).count() == 0 and self.is_child_node():
            return self.parent.get_many_to_many_fallback(relationship)
        return getattr(self, relationship)

    def field_is_inherited(self, field_name):
        """
        Returns True or False depending on whether or not a given field has
        been inherited by a parent.
        """
        return field_name in self._original_field_values
