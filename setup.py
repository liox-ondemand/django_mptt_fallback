# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import os

from setuptools import find_packages, setup

# Allow setup.py to be run from any where. This allows things like the README
# to be found as well as files when running Tox tests.
os.chdir(os.path.dirname(os.path.abspath(__file__)))

with open('README.rst') as readme:
    README = readme.read()

setup(
    name='django_mptt_fallback',
    version='1.0.5',
    description='MPTT Fallback',
    long_description=README,
    url='https://bitbucket.org/liox-ondemand/django_mptt_fallback',
    license='MIT License',
    author='Lionbridge Technologies, Inc.',
    author_email='ondemand@lionbridge.com',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Django>=1.4,<=2.0',
        'django-mptt==0.7.4',
        'django-dirtyfields==0.8.1'
    ],
    test_suite="runtests.runtests",
    classifiers=(
        'Framework :: Django :: 1.8',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
    ),
)
