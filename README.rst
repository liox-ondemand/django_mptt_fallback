=============
Django MPTT Fallback
=============

This package allows you to reduce duplicating data by allowing a child ``MPTTModel`` object to fallback to data from its ancestor.

.. note:: This package is no longer maintained.


Installation
------------

.. code-block:: bash

    $ pip install git+https://bitbucket.org/liox-ondemand/django_mptt_fallback.git


Setup
-----
To begin using django-mptt-fallback, you must create a model that is a subclass of ``MPTTFallbackModel``:

.. code-block:: python

    from django_mptt_fallback.models import MPTTFallbackModel

    class Category(MPTTFallbackModel)

Next, define which fields you'd like to have fallback if they are empty by listing them in ``MPTTFallbackModel.fallback_fields``:

.. code-block:: python
    from django_mptt_fallback.models import MPTTFallbackModel
    
    class Category(MPTTFallbackModel):
        label = models.CharField()
        description = models.CharField()
        
        fallback_fields = (
            'label',
            'description',
        )

If a value is entered in ``MPTTFallbackModel.fallback_fields`` that is not a field on the model,
an AttributeError will be raised on init.

Here is a full example:

.. code-block:: python

    from django.db import models

    from django_mptt_fallback.models import MPTTFallbackModel, MPTTFallbackQuerySet
    from mptt.models import TreeForeignKey
    from mptt.managers import TreeManager

    class Category(MPTTFallbackModel):
        parent = TreeForeignKey(
            'self',
            null=True,
            blank=True,
            related_name='children'
        )
        label = models.CharField(
            max_length=100,
        )
        description = models.CharField(
            max_length=100,
        )
        priority = models.IntegerField(
            blank=True,
            null=True,
        )

        fallback_fields = (
            'description',
            'priority',
        )

        objects = TreeManager.from_queryset(MPTTFallbackQuerySet)()

Example Usage
-------------
.. code-block:: python

    >>> parent = Category.objects.create(label='parent label', description='test description')
    >>> child = Category.objects.create(label='child label', parent=parent)

    >>> parent.description
    >>> 'test description'
    >>> child.description
    >>> 'test description'

Queryset Methods
----------------
MPTTFallbackQuerySet has a ``filter_descendants`` method that will include descendant records that would fallback to parent data for the supplied filters.

Example:

.. code-block:: python

    >>> great_grand_parent = Category.objects.create(label='great grand parent', description='foo')
    >>> grand_parent = Category.objects.create(label='grand parent', parent=great_grand_parent)
    >>> parent = Category.objects.create(label='parent', parent=grand_parent)
    >>> child = Category.objects.create(label='child', description='bar', parent=parent)
    >>> grand_child = Category.objects.create(label='grand child', parent=child)

    >>> Category.objects.filter_descendants(description='foo')
    >>> [great_grand_parent, grand_parent, parent]

    >>> Category.objects.filter_descendants(description='bar')
    >>> [child, grand_child]

Utility Methods
---------------
There are two utility methods that can be used on a ``MPTTFallbackModel`` to affect its fallback data.

``MPTTFallbackModel.empty_fallback_fields()`` will remove the data gather from an object's ancestors and return the model to its true state (as it exists in the database).

``MPTTFallbackModel.gather_fallback_data()`` will populate the object with applicable data from its ancestors.

``MPTTFallbackModel.get_many_to_many_fallback()`` is used to handle accessing many to many fields on
child nodes. This is to safely access and update relationships.

``MPTTFallbackModel.field_is_inherited()`` is used to determine whether or not a given field has been
inherited from a parent.

Example:

.. code-block:: python

    >>> parent = Category.objects.create(label='parent label', description='test description')
    >>> child = Category.objects.create(label='child label', parent=parent)

    >>> parent.description
    >>> 'test description'
    >>> child.description
    >>> 'test description'

    >>> child.empty_fallback_fields()
    >>> child.description
    >>> ''

    >>> child.gather_fallback_fields()
    >>> child.description
    >>> 'test description'

Running Tests
-------------

.. code-block:: bash

    $ python setup.py test
    $ python runtests.py
