from django.db import models

from django_mptt_fallback.models import MPTTFallbackModel, MPTTFallbackQuerySet
from mptt.models import TreeForeignKey
from mptt.managers import TreeManager


class Category(MPTTFallbackModel):
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        related_name='children'
    )
    label = models.CharField(
        max_length=100,
    )
    description = models.CharField(
        max_length=100,
    )
    priority = models.IntegerField(
        blank=True,
        null=True,
    )
    tags = models.ManyToManyField('Tag')

    active = models.NullBooleanField()

    fallback_fields = (
        'description',
        'priority',
        'tags',
        'active',
    )

    objects = TreeManager.from_queryset(MPTTFallbackQuerySet)()


class Tag(models.Model):
    name = models.CharField(
        max_length=100,
    )

    def __unicode__(self):
        return self.name
