from django.test import TestCase

from .models import Category, Tag


class MPTTFallbackModelTests(TestCase):
    def setUp(self):
        tag = Tag.objects.create(name='test tag')
        self.parent = Category.objects.create(
            label='test label',
            description='test description',
            priority=1,
        )
        self.parent.tags.add(tag)
        self.child = Category.objects.create(parent=self.parent)
        self.grand_child = Category.objects.create(parent=self.child)

    def test_fallback(self):
        """
        Ensure that fields marked as fallback_fields fallback all the way up
        the chain
        """
        self.assertEquals(self.parent.description, self.child.description)
        self.assertEquals(self.parent.priority, self.child.priority)
        self.assertEquals(self.parent.description, self.grand_child.description)
        self.assertEquals(self.parent.priority, self.grand_child.priority)

    def test_no_fallback(self):
        """
        Ensure that fields aren't marked as fallback_fields dont'
        fallback all the way up the chain
        """
        self.assertNotEquals(self.parent.label, self.child.label)
        self.assertNotEquals(self.parent.label, self.grand_child.label)

    def test_get_attrs(self):
        """
        Ensure that fallback logic occurs if our getattr utility is used
        """
        expected_value = 'test description'
        self.assertEquals(
            getattr(self.parent, 'description', None),
            expected_value
        )
        self.assertEquals(
            getattr(self.child, 'description', None),
            expected_value
        )
        self.assertEquals(
            getattr(self.grand_child, 'description', None),
            expected_value
        )

    def test_empty_fallback_fields(self):
        """
        Ensure that MPTTFallbackModel.empty_fallback_fields() clears
        out the fields that we want to fall back on.
        """
        self.assertEquals(self.parent.description, self.child.description)
        self.child.empty_fallback_fields()
        self.assertNotEquals(self.parent.description, self.child.description)

    def test_gather_fallback_data(self):
        """
        Ensure that MPTTFallbackModel.gather_fallback_data() populates the
        fields that we want to fall back on.
        """
        self.test_empty_fallback_fields()
        self.child.gather_fallback_data()
        self.assertEquals(self.parent.description, self.child.description)

    def test_repopulating_fields(self):
        """
        Ensure that fields that get fallback fields that get overwritten don't
        reset if we gather_fallback_data() again.
        """
        self.assertEquals(self.parent.description, self.child.description)
        self.child.description = 'updated description'
        self.assertNotEquals(self.parent.description, self.child.description)
        self.child.gather_fallback_data()
        self.assertNotEquals(self.parent.description, self.child.description)

    def test_many_to_many_fallback_error(self):
        """
        Ensure that MPTTFallbackModel.get_many_to_many_fallback() raises
        an attribute error if a non many_to_many field is passed
        """
        with self.assertRaises(AttributeError):
            self.parent.get_many_to_many_fallback('name')

    def test_many_to_many(self):
        """
        Ensure that many_to_many field relationships can be inherited
        """
        new_tag = Tag.objects.create(name='tag two')

        self.assertEquals(
            self.parent.get_many_to_many_fallback('tags').count(),
            self.child.get_many_to_many_fallback('tags').count()
        )

        self.assertEquals(
            self.parent.get_many_to_many_fallback('tags').first(),
            self.child.get_many_to_many_fallback('tags').first()
        )

        self.child.tags.add(new_tag)

        self.assertEquals(
            self.parent.get_many_to_many_fallback('tags').count(),
            self.child.get_many_to_many_fallback('tags').count()
        )

        self.assertNotEquals(
            self.parent.get_many_to_many_fallback('tags').first(),
            self.child.get_many_to_many_fallback('tags').first()
        )

    def test_many_to_many_grand_parent(self):
        """
        Ensure that many_to_many field relationships can be inherited
        """
        new_tag = Tag.objects.create(name='tag two')

        self.assertEquals(
            self.parent.get_many_to_many_fallback('tags').count(),
            self.grand_child.get_many_to_many_fallback('tags').count()
        )

        self.assertEquals(
            self.parent.get_many_to_many_fallback('tags').first(),
            self.grand_child.get_many_to_many_fallback('tags').first()
        )

        self.child.tags.add(new_tag)

        self.assertEquals(
            self.child.get_many_to_many_fallback('tags').count(),
            self.grand_child.get_many_to_many_fallback('tags').count()
        )

        self.assertEquals(
            self.child.get_many_to_many_fallback('tags').first(),
            self.grand_child.get_many_to_many_fallback('tags').first()
        )

    def test_field_is_inherited(self):
        """
        Ensure that MPTTFallbackModel.field_is_inherited() returns the
        correct value.
        """
        self.child.priority = 2
        self.child.save()
        self.child = Category.objects.get(id=self.child.id)
        self.assertTrue(self.child.field_is_inherited('description'))
        self.assertFalse(self.child.field_is_inherited('priority'))

    def test_child_has_same_value_as_parent(self):
        """
        Ensure that setting the same value as a parent field
        to a child field keeps the new value
        """
        self.child.priority = self.parent.priority
        self.child.save()
        self.child = Category.objects.get(id=self.child.id)
        self.child.empty_fallback_fields()
        self.assertEquals(self.child.priority, 1)


class MPTTFallbackQuerySetTests(TestCase):
    def setUp(self):
        self.grand_parent = Category.objects.create(
            label='test label',
            description='description'
        )
        self.parent = Category.objects.create(
            label='test label',
            description='test description',
            parent=self.grand_parent,
            active=True
        )
        self.child = Category.objects.create(parent=self.parent, priority=1)
        self.grand_child = Category.objects.create(parent=self.child)
        self.great_grand_child = Category.objects.create(
            description='grand child description',
            parent=self.child,
            active=False
        )
        self.great_great_grand_child = Category.objects.create(
            parent=self.great_grand_child
        )

    def test_filter_descendants_fallback_field(self):
        """
        Ensure that filtering by a fallback field returns descendants
        with an empty value.
        """
        results = Category.objects.filter_descendants(
            description='test description'
        )
        self.assertEquals(results.count(), 3)

    def test_filter_descendants_fallback_field_same_value(self):
        """
        Ensure that filtering by a fallback field returns descendants
        with an empty value and descendants with the value explicitly set
        """
        self.child.description = 'test description'
        self.child.save()
        results = Category.objects.filter_descendants(
            description='test description'
        )
        self.assertEquals(results.count(), 3)

    def test_filter_descendants_multiple_fallback_field(self):
        """
        Ensure that filtering by multiple fallback fields returns
        the correct results.
        """
        results = Category.objects.filter_descendants(
            description='test description',
            priority=1
        )
        self.assertEquals(results.count(), 2)

    def test_filter_descendants_non_fallback_field(self):
        """
        Ensure that filter by non fallback fields does not return descendants
        with an empty value.
        """
        results = Category.objects.filter_descendants(label='test label')
        self.assertEquals(results.count(), 2)

    def test_filter_descendants_boolean_field(self):
        """
        Ensure that filter_descendants doesn't return descendants
        with an empty value as False
        """
        results = Category.objects.filter_descendants(active=False)
        self.assertEquals(results.count(), 2)

    def test_filter_descendants_boolean_inherit_true(self):
        """
        Ensure that a child category inheriting a true value for
        a boolean field is included in the results
        """
        results = Category.objects.filter_descendants(active=True)
        self.assertIn(self.child, results)

    def test_filter_descendants_boolean_inherit_false(self):
        """
        Ensure that a child category inheriting a false value for
        a boolean field is included in the results
        """
        results = Category.objects.filter_descendants(active=False)
        self.assertIn(self.great_great_grand_child, results)
