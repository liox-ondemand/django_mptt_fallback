import sys
from os.path import dirname, abspath
from optparse import OptionParser

from django.conf import settings
import django


def runtests(*test_args, **kwargs):
    BASE_DIR = dirname(abspath(__file__))
    if not settings.configured:
        settings.configure(
            DATABASES={
                'default': {
                    'ENGINE': 'django.db.backends.sqlite3',
                }
            },
            INSTALLED_APPS=(
                'tests',
                'django_mptt_fallback'
            ),
        )
    django.setup()
    if not test_args:
        test_args = ['tests']

    sys.path.insert(0, BASE_DIR)
    from django.test.utils import get_runner
    test_runner = get_runner(settings)(
        verbosity=kwargs.get('verbosity', 1),
        interactive=kwargs.get('interactive', False),
        failfast=kwargs.get('failfast'),
    )
    test_runner.setup_test_environment()
    failures = test_runner.run_tests(test_args)
    sys.exit(failures)

if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option(
        '--failfast',
        action='store_true',
        default=False,
        dest='failfast'
    )

    (options, args) = parser.parse_args()

    runtests(failfast=options.failfast, *args)
